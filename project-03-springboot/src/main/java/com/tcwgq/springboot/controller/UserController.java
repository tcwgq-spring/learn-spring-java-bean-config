package com.tcwgq.springboot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/user")
public class UserController {

    /***
     * 用户列表
     */
    @GetMapping(value = "/list")
    public String list() {
        return "user_list";
    }

}
