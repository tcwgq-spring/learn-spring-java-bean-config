package com.tcwgq.springsessionmvc.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author tcwgq
 * @since 2022/6/27 17:22
 **/
@RestController
@RequestMapping("/r")
public class ResourceController {
    /**
     * 测试资源1
     */
    @GetMapping(value = "/r1")
    public String r1() {
        return "访问资源1";
    }

    /**
     * 测试资源2
     */
    @GetMapping(value = "/r2")
    public String r2() {
        return "访问资源2";
    }

}
