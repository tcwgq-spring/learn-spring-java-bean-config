package com.tcwgq.springsessionmvc.service;

import org.springframework.stereotype.Service;

/**
 * @author tcwgq
 * @since 2022/8/2 11:53
 */
@Service
public class AService {
    public String hello() {
        return "Hello";
    }

}
