package com.tcwgq.springsessionmvc.service;

import com.tcwgq.springsessionmvc.bean.User;
import org.springframework.stereotype.Service;

/**
 * @author tcwgq
 * @since 2022/8/2 11:52
 */
@Service
public class UserService {
    public User info() {
        return new User("zhangSan", "123");
    }

}
