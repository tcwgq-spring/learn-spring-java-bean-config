package com.tcwgq.springsessionmvc.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.stereotype.Controller;

/**
 * @author tcwgq
 * @since 2022/8/2 11:52
 */
@Configuration
@ComponentScan(value = "com.tcwgq.springsessionmvc", includeFilters = {
        @ComponentScan.Filter(type = FilterType.ANNOTATION, classes = Controller.class)
}, useDefaultFilters = false)
public class Config {

}
