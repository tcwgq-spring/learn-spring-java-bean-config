package com.tcwgq.springsessionmvc;

import com.tcwgq.springsessionmvc.config.Config;
import com.tcwgq.springsessionmvc.service.AService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author tcwgq
 * @since 2022/8/1 23:17
 */
public class ApplicationContextTest {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Config.class);
        AService a = context.getBean(AService.class);
        String hello = a.hello();
        System.out.println(hello);
    }

}
